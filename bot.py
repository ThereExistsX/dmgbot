# bot.py
import discord

import os
from dotenv import load_dotenv

load_dotenv()

TOKEN = os.getenv('DISCORD_TOKEN')

class dmgClient(discord.Client):
	async def on_ready(self):
		print(f'{self.user} has connected to Discord!')

	async def on_member_join(self, member):
		await member.send('Please choose a starter!')

	async def on_message(self, message):
		if message.author == self.user:
			return

		if message.channel.id == message.author.dm_channel.id:
			roleCommand = message.content.lower()

			if roleCommand == 'bulbasaur':
				#await message.author.send('you picked the grass type')
				# await message.author.add_roles('bulbasaur')
				server = self.get_guild(int(os.getenv('SERVER_ID')))
				role = discord.utils.get(server.roles, name='bulbasaur')
				member = server.get_member(message.author.id)
				await member.add_roles(role)
				await message.author.send('your starter is bulbasaur')
				print('the grass type')

			elif roleCommand == 'charmander':
				print('the fire type')
			elif roleCommand == 'squirtle':
				print('the water type')
			else:
				print('thats not a starter pokemon!')
		

intents = discord.Intents.default()
intents.members = True

client = dmgClient(intents=intents)
client.run(TOKEN)
